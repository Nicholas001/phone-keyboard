package com.nicholas.keyboard.app;

import com.nicholas.keyboard.logics.Logics;

import java.util.Scanner;

public class AppRunner {

    public static void main(String[] args) {

        Logics obj = new Logics();
        obj.displayKeys();

        System.out.println("Please insert number combination!");
        Scanner nr = new Scanner(System.in);
        int number = nr.nextInt();
        obj.combinations(number);
        obj.displaySolution();
    }
}
