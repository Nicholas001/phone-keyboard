package com.nicholas.keyboard.logics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Logics {

    private Map<String, String> keyBoard = new <String, String>HashMap() {{
//      aici sunt adaugate in Map lista cu literele tastelor,
//      unde cheia este cifra iar valoarea este combinatia de litere
        put("1", "abc");
        put("2", "def");
        put("3", "ghi");
        put("4", "jkl");
        put("5", "mno");
        put("6", "pqr");
        put("7", "stv");
        put("8", "uxy");
        put("9", "wz");
    }};

    public void displayKeys() {
//        Aici sunt afisate literele pentru fiecare dintre taste
        System.out.println(" 1      2      3");
        System.out.println("abc    def    ghi");
        System.out.println(" 4      5      6");
        System.out.println("jkl    mno    pqr");
        System.out.println(" 7      8      9");
        System.out.println("stv    uxy     wz");
        System.out.println("_________________");
    }

    private ArrayList<String> solution = new ArrayList<String>() {{
        add("");
    }};

    public void combinations(int x) {
        //Avem o metoda recursiva care adauga intru-un string combinatiile de litere posibile
        //este declarat un string in care este stocat numarul (combinatia de cifre introdusa de la tastatura)
        String phoneNumber = String.valueOf(x);

        if (x != 0) {
//          Este declarat un arraylist de stringuri, temporar, pentru a stoca stringul deja creat
//          pentru ca ulterior acesta sa fie curatat pentru a se aduga noi valori
            ArrayList<String> helpArray = new ArrayList<>(solution);
//          lista solutie este golita
            solution.clear();
//          se intereaza prin lista de solutii pentru a se vedea cate combinatii au fost deja create
            for (int y = 0; y < helpArray.size(); y++) {
                int temp = (keyBoard.get(String.valueOf(phoneNumber.charAt(0)))).length();
//          se intereaza prin sirul de litere de pe o singura tasta si pentru fiecare litera gasita
//          aceasta se concateneaza la solutiile deja avute
                for (int z = 0; z < temp; z++) {
                    String text = keyBoard.get(String.valueOf(phoneNumber.charAt(0)));
                    char t = text.charAt(z);
                    text = String.valueOf(t);
                    solution.add(helpArray.get(y).concat(text));
                }
            }
//          printr=o metoda ajutatoare se renunta la prima cifra din numarul introdus de la tastatura
            x = helpMethod(x);
//          apelul recursiv
            combinations(x);
        }

    }

    public int helpMethod(int x) {

        if (x > 10) {
            String phoneNumber = String.valueOf(x);
//      prin metoda substring din clasa String se renunta la prima cifra din numarul introdus de la tastatura
            phoneNumber = phoneNumber.substring(1);
            x = Integer.parseInt(phoneNumber);
            return x;
        } else {
            return 0;
        }
    }

    public void displaySolution() {
        System.out.println(solution.toString());
        System.out.println("Numarul de combinatii posibile este de " + solution.size() + ".");
    }


}
